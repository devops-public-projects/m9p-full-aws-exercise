#!/usr/bin/env bash

export IMAGE=$1
echo IMAGE > /home/ec2-user/previous-image-name
docker-compose -f docker-compose.yaml up --detach
echo "Ran server commands"