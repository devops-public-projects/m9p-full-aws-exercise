
# Complete CI/CD Pipeline to Deploy a NodeJS App on an EC2 Instance
In this project we will build a full CI/CD pipeline and configure an AWS EC2 instance for us to deploy to.

## Technologies Used
- AWS
- Jenkins
- Docker
- Docker Hub
- Linux
- Git
- NodeJS
- NPM

## Project Description
- Set up AWS CLI, Users, Groups
- Create a new VPC
- Create an EC2 Instance
- Install Docker on EC2 Instance
- Configure a **docker-compose.yaml** for our NodeJS app
- Configure Jenkins to automatically trigger from Git changes and to deploy to our EC2 Instance

## Prerequisites
- AWS Account
- [Git Project cloned into a personal repository](https://gitlab.com/twn-devops-bootcamp/latest/09-aws/aws-exercises)

## Guide Steps
### EXERCISE 1: Create IAM user
- Create IAM Group
	- `aws iam create-group --group-name devops`

- Assign Policies to Group
	- `aws iam attach-group-policy --group-name devops --policy-arn arn:aws:iam::aws:policy/AmazonEC2FullAccess`
	- `aws iam attach-group-policy --group-name devops --policy-arn arn:aws:iam::aws:policy/AmazonVPCFullAccess`

- Create User:
	- `aws iam create-user --user-name chris`

- Add Group to User
	- `aws iam add-user-to-group --user-name chris --group-name devops`

- Create Key Pair | SSH Access to Server
	- `aws ec2 create-key-pair --key-name chrisKeyPair --query 'KeyMaterial' --output text > chrisKeyPair.pem`
		- `mv chrisKeyPair.pem ~/.ssh`
		- `chmod 400 ~/.ssh/chrisKeyPair.pem`

![Group and user created](/images/m9p-group-and-user-created.png)


### EXERCISE 2: Configure AWS CLI
- Generate Key-Pair for User | AWS CLI Access
	- `aws iam create-access-key --user-name chris`

- Configure AWS
	- `aws configure`
		- Access ID: `ACCESS_ID`
		- Secret Key: `SECRET_KEY`
		- Region: `us-east-1`
		- Output Format: `json`

### EXERCISE 3: Create VPC
- Create VPC
	- `aws ec2 create-vpc --cidr-block 172.31.0.0/16`

- Create Internet Gateway
	- `aws ec2 create-internet-gateway`
		- Gives you a `igw-XXXXX`

- Add Route table
	- `aws ec2 create-route --route-table-id rtb-XXXXX --destination-cidr-block 0.0.0.0/0 --gateway-id igw-XXXXX`

- Attach Gateway to VPC
	- `aws ec2 attach-internet-gateway --internet-gateway-id GATEWAY_ID --vpc-id vpc-XXXXX`

- Create Subnet
	- `aws ec2 create-subnet --cidr-block 172.31.0.0/20 --vpc-id vpc-XXXXX`
	- `aws ec2 modify-subnet-attribute --subnet-id subnet-XXXXX --map-public-ip-on-launch`

![VPC Created](/images/m9p-vpc-created.png)

- Create Security Group for VPC
	- `aws ec2 create-security-group --group-name devops-security-group --description "DevOps Security Group" --vpc-id vpc-XXXXX`
- Add SSH Access
	- `aws ec2 authorize-security-group-ingress --group-id sg-XXXXX --protocol tcp --port 22 --cidr YOUR_IP/32`
	- SSH Access for Jenkins Server
		- `aws ec2 authorize-security-group-ingress --group-id sg-XXXXX --protocol tcp --port 22 --cidr JENKINS_IP/32`

![Configured VPC](/images/m9p-vpc-configured.png)

### EXERCISE 4: Create EC2 Instance
- Image-id: **Latest Amazon Linux Image**
- Key-name:**From previous step**
- Security-Group-Id: **From previous step**
- Subnet-Id: One from the list from `aws ec2 describe-subnets`
```
aws ec2 run-instances \
--image-id ami-0230bd60aa48260c6 \
--count 1 \
--instance-type t2.micro \
--key-name chrisKeyPair \
--security-group-ids sg-07eb51c751c424a0c \
--subnet-id subnet-0ea1e2e0326c949df
```
![Instance Created and Running](/images/m9p-instance-running.png)

### EXERCISE 5: SSH into the server and install Docker on it
- `ssh -i ~/.ssh/chrisKeyPair.pem ec2-user@INSTANCE_PUBLIC_IP`
- Install Docker
	- `sudo yum update`
	- `sudo yum install docker`
	- `sudo service docker start`
	- `sudo usermod -aG docker $USER`
	- `exit`
	- `ssh -i ~/.ssh/chrisKeyPair.pem ec2-user@INSTANCE_PUBLIC_IP`
- Install Docker-Compose
	- `sudo curl -L https://github.com/docker/compose/releases/latest/download/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose`
	- `sudo chmod +x /usr/local/bin/docker-compose`
---
**Note**: Make sure to have the cloned repo listed in the **Prerequisites** section and create a new Multi-branch pipeline in Jenkins that links to this repository.

### EXERCISE 6: Add docker-compose for deployment
- `vim docker-compose.yaml`
	- We obtained the project name from the `package.json` in the app folder. We also found the port for the app in the `server.js` file.
```yaml
version: '3.8'
	services:
		bootcamp-node-project:
			image: ${IMAGE}
			ports:
				- "3000:3000"
```
- `vim Dockerfile`

```dockerfile
FROM node:20-alpine
RUN mkdir -p /home/node-app
COPY ./app /home/node-app
WORKDIR /home/node-app
RUN npm install
EXPOSE 3000
CMD ["node","server.js"]
```

### EXERCISE 7: Add "deploy to EC2" step to your existing pipeline
- I'll utilize a Jenkins Shared Library script but I'll leave all the raw code in the Jenkinsfile.
```groovy
 echo "Deploying the application..."
 def shellCmd = "bash ./server-cmds.sh ${IMAGE_NAME}"
 def ec2Instance = "ec2-user@EC2_IP"
  
 sshagent(['ec2-server-key']) {
 sh "scp server-cmds.sh ${ec2Instance}:/home/ec2-user"
 sh "scp docker-compose.yaml ${ec2Instance}:/home/ec2-user"
 sh "ssh -o StrictHostKeyChecking=no ${ec2Instance} ${shellCmd}"
 }
```
- Configure Jenkins private key for EC2 access as ec2-user
	- Jenkins Pipeline > Credentials > pipeline scope > Global Credentials > Add Credentials
		- Type: **SSH username with private key**
		- ID: **ec2-server-key**
		- Username: **ec2-user**
		- Private Key: Contents of the **.pem** created earlier in the **~/.ssh** directory
		- **Create**

### EXERCISE 8: Configure access from browser (EC2 Security Group)
- Add App Access Rule
	- `aws ec2 authorize-security-group-ingress --group-id sg-XXXXX --protocol tcp --port 3000 --cidr YOUR_IP/32`

### EXERCISE 9: Configure automatic triggering of multi-branch pipeline
#### GitLab Section
- Open Repository for project
	- Settings > Integrations > Jenkins > Configure
		- Enable Integration: **Active**
		- Trigger: **Push**
		- Jenkins URL: **JENKINS_IP:PORT**
		- SSL Verification: **Off**
		- Project Name: **Jenkins Project Name**
		- Username: **Username of Jenkins user**
		- Password: **Password of Jenkins user**
		- **Test Changes** > **Save Changes**
	-	Settings > Webhooks > Add new webhook
		-	URL: **http://JENKINS_IP:PORT/multibranch-webhook-trigger/invoke?token=my-project**
		-	Trigger: **Push Events** > **All branches**
		-	SSL Verification: **Off**
		-	**Test** > **Save changes**

#### Jenkins Section
- Pipeline > Configure > Scan Multibranch Pipeline Triggers > Scan by Webhook
	- Token: **my-project**
	- **Save**

- Any pushes to the repository will now trigger a build in Jenkins for that multibranch pipeline.
