#!/usr/bin.env groovy

library identifier: 'jenkins-shared-library@master', retriever: modernSCM(
        [$class: 'GitSCMSource',
         remote: 'https://gitlab.com/devops-public-projects/jenkins-shared-library.git',
         credentialsId: 'gitlab-credentials'
        ]
)

pipeline {
    agent any
    tools {
        nodejs 'my-nodejs'
    }
    stages {
        stage('increment version') {
            steps {
                script {
                    echo "Incrementing the App Version"
                    sh 'npm --prefix ./app version patch'

                    def packageJsonText = sh(script: 'cat ./app/package.json', returnStdout: true).trim()
                    def packageJson = readJSON text: packageJsonText

                    env.IMAGE_NAME = "theinstinct/demo-app:${packageJson.version}-${BUILD_NUMBER}"
                }
            }
        }
        stage('package app') {
            steps {
                echo "Building the Application"
                sh 'npm pack app/'
            }
        }
        stage('build docker image') {
            steps {
                script {
                    buildImage(env.IMAGE_NAME)
                    dockerLogin()
                    dockerPush(env.IMAGE_NAME)
                }
            }
        }
        stage("deploy") {
            steps {
                script {
                    echo "Deploying the application..."
                    def shellCmd = "bash ./server-cmds.sh ${IMAGE_NAME}"
                    def ec2Instance = "ec2-user@3.89.81.162"

                    sshagent(['m9p-ec2-user']) {
                        sh "scp server-cmds.sh ${ec2Instance}:/home/ec2-user"
                        sh "scp docker-compose.yaml ${ec2Instance}:/home/ec2-user"
                        sh "ssh -o StrictHostKeyChecking=no ${ec2Instance} ${shellCmd}"
                    }
                }
            }
        }
        stage("commit version update") {
            steps {
                script {
                    echo "Committing Version Increment"

                    sh 'git config --global user.email "jenkins@example.com"'
                    sh 'git config --global user.name "jenkins"'

                    withCredentials([usernamePassword(credentialsId: 'gitlab-credentials', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
                        sh "git remote set-url origin https://${USER}:${PASS}@gitlab.com/devops-public-projects/m9p-nodejs-project.git"
                        sh 'git add .'
                        sh 'git commit -m "Jenkins: Version Bump"'
                        sh 'git push origin HEAD:${BRANCH_NAME}'
                    }

                }
            }
        }
    }
}
