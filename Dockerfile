FROM node:20-alpine

RUN mkdir -p /home/node-app

COPY ./app /home/node-app

WORKDIR /home/node-app

RUN npm install

EXPOSE 3000

CMD ["node","server.js"]